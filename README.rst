Userscripts
===========

Collection of userscripts to make corporate tools slightly less annoying to
use.

rh-jira
-------

`rh-jira-comments <rh-jira-comments.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Automatically load all comments for an issue.

`rh-jira-timestamps <rh-jira-timestamps.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Replace the 'relative' timestamps with the actual timestamp
in local timezone and 24h format.

`rh-jira-sidebar <rh-jira-sidebar.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Move the 'users' sidebar next to the 'Details' section without
taking space next to the rest of the page. Useful for users of
narrow browser windows (portrait monitor).

`rh-jira-monospace-comments <rh-jira-monospace-comments.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Override fonts for comments and description to use the default
monospace fonts. Useful to be able to read command outputs.

`rh-jira-sso-preserve-navigation <rh-jira-sso-preserve-navigation.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Store and preserve the redirection when logging in via SSO. Useful if SSO fails
e.g. because you're already logged in to preserve the navigation inside Jira.

See below for scripts to recover from SSO failure.

SSO login
---------

`rh-sso-autologin <rh-sso-autologin.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Automatically confirm email address to automate SSO login flow into Jira. Don't
forget to modify the email address.

`rh-sso-autologin-error <rh-sso-autologin-error.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Recover from login failures by navigating back to Jira. The complementary script
to restore navigation inside Jira is needed.

other
-----

`rh-workday <rh-workday.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Bypass the pointless 15 minute session timeout. Note that a server side 24h
session duration still applies.

`rh-adp <rh-adp.user.js>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Open directly the more useful page in ADP and make the view wider.


Userscript development
======================

Firefox doesn't allow addons to load local files so a workaround offered by
Tampermonkey is to load a file from a local http server. The local server
provided in ``server.py`` disables caching as that interferes with the updates.

1) Run the server::

 $ python server.py

2) open the local server page

 http://localhost:8080

3) click on script to develop

4) Select ``Track local file before this window is closed``

5) **DO NOT CLOSE THE WINDOW**

The script will be auto-loaded periodically so you can develop it in the local
editor.
