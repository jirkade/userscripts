// ==UserScript==
// @name        Red Hat SSO autologin error recovery
// @namespace   Violentmonkey Scripts
// @match       https://sso.redhat.com/auth/realms/redhat-external/broker/after-post-broker-login
// @match       https://sso.redhat.com/auth/realms/redhat-external/broker/auth.redhat.com/endpoint
// @match       https://sso.redhat.com/auth/realms/redhat-external/login-actions/post-broker-login
// @match       https://sso.redhat.com/auth/realms/redhat-external/login-actions/authenticate
// @grant       none
// @version     1.0.0
// @author      Peter Krempa
// @description 2023/09/21
// @run-at      document-end
// ==/UserScript==

(function(){
    "use strict";

    /* If one of the above SSO phases gets stuck for too long, which usually
     * happens when it displays an error during the auth flow or the
     * "Already logged in" message redirect to JIRA, as in most cases we're
     * already logged in from another tab at that point. */
    setTimeout(function(){
        window.location = 'https://issues.redhat.com/browse/RHEL-1';
    }, 5000);
})();
