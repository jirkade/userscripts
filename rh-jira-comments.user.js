// ==UserScript==
// @name        rh-jira-comments
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       GM_addStyle
// @version     1.0
// @author      Peter Krempa
// @description 2023/09/13
// @run-at      document-end
// ==/UserScript==

(function(){
    "use strict";

    /* Load all comments by shift-clicking the button once it appears. */
    function loadComments() {
        let loadButtons = document.querySelectorAll('.show-more-tab-items:not(.alreadyClickedByUserscript)');
        let ev = new MouseEvent("click", { bubbles: true, cancelable: false,  shiftKey: true, buttons: 1});

        for (const but of loadButtons) {
            but.dispatchEvent(ev);
            but.classList.add('alreadyClickedByUserscript');
        }
    }

    let mut = new MutationObserver(loadComments);
    mut.observe(document.documentElement, { subtree: true, childList: true });

})();
