// ==UserScript==
// @name        rh-jira-sidebar
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       GM_addStyle
// @version     1.1
// @author      Peter Krempa
// @description 2023/09/13
// ==/UserScript==

(function(){
    "use strict";

    /* Move sidebar next to 'Details' so that bottom of the page can be wider.
     * The mutation observer also reapplies the change for any dynamically
     * loaded issue content which can happen e.g. when opening issues in a list */
    function moveSidebar() {
        let issueContent = document.querySelectorAll('#issue-content:not(.customMovedSidebar)');

        for (const el of issueContent) {
            let sidebar = el.querySelector('#viewissuesidebar');
            let main = el.querySelector('.issue-main-column');
            let details = el.querySelector('#details-module');

            if (details && sidebar && main) {
                main.prepend(sidebar);
                main.prepend(details);

                el.classList.add('customMovedSidebar');
            }
        }
    }

    let mut = new MutationObserver(moveSidebar);
    mut.observe(document.documentElement, { subtree: true, childList: true });

    moveSidebar();
})();


GM_addStyle (`

/* Right sidebar */
    #details-module {
        display: inline-block;
        width: calc(70% - 1em);
        vertical-align: top;
    }

    #viewissuesidebar {
        display: inline-block;
        width: auto;
        max-width: 30%;
        margin-left: 1em;
        padding: 0px;
        vertical-align: top;
    }
` );
