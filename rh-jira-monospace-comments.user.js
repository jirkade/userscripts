// ==UserScript==
// @name        rh-jira-monospace-comments
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       GM_addStyle
// @version     1.0
// @author      Peter Krempa
// @description 2023/09/13
// ==/UserScript==

GM_addStyle (`

/* Font stuff */

    /* Use browser-defined sans-serif font, instead of forced ones. */
    html
    {
        font-family: sans-serif;
    }

    /* Use monospace font for comment body and issue description. */
    .action-body,
    #description-val
    {
        font-family: monospace;
    }
` );
