// ==UserScript==
// @name        rh-jira restore navigation URI after SSO flow
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       none
// @version     1.0.0
// @author      Peter Krempa
// @description 2024/9/21
// @run-at      document-start
// ==/UserScript==

(function(){
    "use strict";

    /* Remember the 'redirecTo' argument in the session local store when
     * navigation takes us to:
     *
     *  https://issues.redhat.com/plugins/servlet/samlsso
     *
     * which is an automatic redirect to login via the sso flow. This is needed
     * in case when the sso login flow fails, which happens mostly because
     * we're already logged in.
     *
     * We can then restore the window.location to where we intended to navigate
     * after being redirected back from the SSO flow.
     */

    if (window.location.pathname.includes('servlet/samlsso')) {
        /* remember the content of 'redirectTo' in session local store */
        window.location.search.substr(1).split('&').forEach((element) => {
            let pair = element.split('=');

            if (pair[0] == 'redirectTo')
                window.sessionStorage.setItem('rememberRedirectTo', pair[1]);
        })
    } else {
        let oldNav = window.sessionStorage.getItem('rememberRedirectTo');
        let decoded = decodeURIComponent(oldNav);

        window.sessionStorage.removeItem('rememberRedirectTo');

        if (oldNav != null &&
            document.referrer.includes('sso.redhat.com')) {
            let href = "https://issues.redhat.com" + decoded;

            if (window.location != href) {
                window.location = href;
            }
        }
    }
})();
