// ==UserScript==
// @name           Gmail Fixed Font
// @namespace      http://www.indelible.org/
// @description    Fixed-font message bodies for Gmail
// @author         Jon Parise, James Tunnicliffe
// @version        1.3
// @include        http://mail.google.com/*
// @include        https://mail.google.com/*
// @include        http://*.mail.google.com/*
// @include        https://*.mail.google.com/*
// @grant          GM_addStyle
// ==/UserScript==

GM_addStyle (`
.ii, .Ak
{ 
    font: medium monospace !important;
}

.a3s
{
    font: medium monospace !important;
}

.editable
{
    font: medium monospace !important;
}

span[aria-label*="Suggested chat"]
{
    display: none !important;
}

`);
