// ==UserScript==
// @name         RHBZ sanitize style
// @version      1.0
// @author       Peter Krempa
// @match        https://bugzilla.redhat.com/show_bug.cgi
// @match        https://bugzilla.redhat.com/process_bug.cgi
// @grant        GM_addStyle
// ==/UserScript==

(function() {
    'use strict';
    
    // icon - works now
    var icon = document.createElement('link');
    icon.href = 'https://www.redhat.com/profiles/rh/themes/redhatdotcom/favicon.ico';
    icon.rel = 'shortcut icon';
    document.head.appendChild(icon);

    // make textareas narrower
    $("textarea").each(function() { $(this).attr("cols", 50); });
  
    GM_addStyle (`
/* replace suboptimal choice of fonts by browser's 'sans-serif' */
body, td, th, input, dt, button, h1, h2, h3, h4, h5, h6, label, .bz_section_title, .tabs.tabs td 
{
    font-family: sans-serif !important;
}

/*
 *  Hide unnecessary and obstructing elements
 */

/* horizontal lines are replaced by styling */
hr,
/* the "minor update" checkbox and title */
label[for=minor_update],
input[name=minor_update],
/* time tracking table is not used by our team */
.bz_time_tracking_table,
/* */
.instructions,
.bz_collapse_expand_comments,
li.submenu:nth-child(6),
li.submenu:nth-child(7), 
li.submenu:nth-child(8),
a[title="Quicksearch Help"],
#titles,
#external_bug_last,
#footer,
#votes_container,
#bvp_toggle_editor,
#bugzilla-body > br,
ul.related_actions:nth-child(2),
.navigation,
td.bz_flags + td a,
.fetch_all,
#bz_component_input + a,
a[title='tag comment'],
.bz_collapse_comment,
a[title="Export as XML"],
a[title="Format For Printing"],
a[title="TreeView+"],
a[title="Top of page"],
#add_comment > table > tbody > tr > td > br + br,
caption#et0
{
    display: none !important;
}



body
{
    background: #eaeaea;
}


.asdfasdfasdfasdfasdf
{
    background: red;
}

#header {
    margin-bottom: 0;
}

#header #banner {
    margin-right: 0;
}

#header #banner .links {
    margin-top: 0;
    background-color: inherit;
}

table.edit_form,
#bz_big_form_parts
{
    background: #ffffff;
    padding: 0.5em;
    border: 1px #a0a0a0 solid;
    margin-bottom: 1em;
}

table.edit_form
{
    margin-bottom: 0px;
    border-bottom: none;
}

#bz_big_form_parts
{
    border-top: 1px #a0a0a0 dashed;
    padding-left: 1em;
}

.bz_comment
{
    background: #ffffff;
    border: 1px #a0a0a0 solid;
    margin-bottom: 1em;
}

.bz_comment_text
{
    width: inherit !important;
    padding-bottom: 0px;
    margin: 0px;
    margin-left: 1em;
    margin-right: 1em;
}

.bz_private
{
	background: #f3eeee;
}

.ih_inlinehistory
{
    margin-left: 0px;
    padding-left: 1em;
}

.bz_section_additional_comments
{
    background: #ffffff;
    border: 1px #a0a0a0 solid;
    border-bottom: 1px #a0a0a0 dashed;
    padding: 0.5em;
    padding-bottom: 0px;
}

.related_actions
{
    background-color: #ffffff;
    width: inherit;
    float: none;
    font-size: 1em;
    padding: 0.3em;
    border: 1px #a0a0a0 solid;
    border-top: none;
    margin-bottom: 1em;
}

.ih_history
{
    background: #ffffff;
}

.ih_history_item
{
    background: #ffffff;
    margin-bottom: 0px;
}

.ih_history_change
{
    background: #ffffff;
    top-padding: 0.3em;
}

input#cf_fixed_in 
{
     border-radius: 0px;
}

.bz_short_desc_container,
.bz_bug .bz_short_desc_container
{
    display: block;
    border-radius: 0px;
    border: 1px #a0a0a0 solid;
    padding: 7px;
    padding-bottom: 7px;
}

.bz_bug .edit_form {
	width: inherit !important;
}

/* make bz comments fill up entire width of the main container */
.bz_comment_table > tbody > tr > td,
.bz_comment_table > tbody > tr,
.bz_comment_table > tbody,
.bz_comment_table
{
    width: 100%;
    padding: 0px;
    display: block;
}

#header .links a, #banner a,
#header .form .btn 
{
    font-size: 100%;
}

#bugzilla-body th,
.field_label
{
    font-weight: normal;
}

#status,
#comment
{
    margin: 0px;
}

/* input boxes should not be rounded */
.selectize-input,
.text_input,
.bz_userfield,
input[id="needinfo_form"],
input,
textarea {
    border-radius: 0px;
    border: solid 1px #c0c0c0;
}

textarea
{
    width: 100%;
}

#add_comment > table
{
	width: 100%;
}

#bz_big_form_parts > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1)
{
    width: 100%;
}

a[title="Copy is a lite weight clone that only copies the summary & description"]::before {
    content: "Light clone ";
    padding-left: 0.5em;
}
    
a[title="Clone This Bug"]::before {
    content: "Clone ";
    padding-left: 0.5em;
}

` );
})();
