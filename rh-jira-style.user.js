// ==UserScript==
// @name        rh-jira-style
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       GM_addStyle
// @version     1.0
// @author      Peter Krempa
// @description 2023/09/13
// ==/UserScript==

GM_addStyle (`

/* Removal of useless elements */

    /* remove collapse buttons from comments (comments are expanded on-load) */
    .activity-comment > .actionContainer > .action-head > button
    {
        display: none;
    }

    /* remove other collapse buttons  (clicking the heading collapses) */
    .module > .mod-header > button
    {
        display: none;
    }

    /* hide header of 'Activity' section completely */
    #activitymodule_heading
    {
        display: none;
    }

/* styling changes - comments */

    /* border and padding/margins */
    .activity-comment
    {
        border: 1px #505050 solid;
        padding: 0em;
        margin-bottom: 1em;
    }

    /* disable padding on internal container */
    .activity-comment > .actionContainer
    {
        padding: 0px;
    }

    /* gray backround for comment header */
    .activity-comment > .actionContainer > .action-head
    {
        background-color: #dadada;
    }

    /* white backround for comment body and links */
    .activity-comment > .actionContainer > .action-body,
    .activity-comment > .actionContainer > .action-links
    {
        background-color: #ffffff;
    }


    /* space out contents */
    .activity-comment > .actionContainer > .action-head,
    .activity-comment > .actionContainer > .action-body,
    .activity-comment > .actionContainer > .action-links
    {
        margin-top: 0px;
        padding: 0.3em 0.5em 0.3em 0.5em;
    }

/* styling changes */

    /* left sidebar darkening */
    :root {
        --aui-sidebar-bg-color: #c0c0c0;
    }

    /* override padding before modules */
    .issue-body-content .module > .mod-header + .mod-content
    {
        padding-left: 0px;
    }

    /* main background darkening */
    #issue-content,
    .issue-header,
    .issue-view
    {
        background-color: #d0d0d0 !important;
    }

    /* module header fixing */
    .mod-header
    {
        padding: 0.3em 0.5em 0.3em 0.5em !important;
        background-color: #dadada;

    }

    .mod-header > h4
    {
        background-color: inherit !important;
    }

    /* module content fixing */
    .mod-content
    {
        margin-top: 0px !important;
        padding: 0.5em !important;
    }

    #attachmentmodule .mod-content.issue-drop-zone
    {
        margin: 5px !important;
    }


    /* border, padding and white background for all modules except 'activity' */
    .module
    {
        border-top: 1px #505050 solid !important;
        padding-top: 0px !important;
        background-color: #ffffff;
        border: 1px #505050 solid;

    }

    /* override above for activity - dashed border */
    #activitymodule
    {
        border-top: 1px #e0e0e0 dashed !important;
        border: 1px #e0e0e0 dashed;
        background-color: #d0d0d0;
    }

    /* override above for activity and addcoment */
    #addcomment-inner,
    #addcomment
    {
        border-top: none !important;
        border: none !important;
        background-color: #d0d0d0;
    }


    /* issue header background and border */
    .issue-header-content > .aui-page-header
    {
        background-color: #ffffff !important;
        padding: 0.5em !important;
        padding-bottom: 0.5em !important;
        margin: 15px 20px 0px;
        border: 1px #505050 solid;
    }

` );
